/*
Ciclocomputador Baseado em Arduino
Rachel Mendes Nepomuceno
Engenharia de Telecomunicações - 2015.1
*/

#include <TimerOne.h>
#include <EEPROM.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_GFX.h>
#include <TTP229.h>

#define OLED_RESET 4

#define STOPPED 0 //definição de estado: parado
#define WAITING_S2A 1 //definição de estado: esperandoSensor2A
#define WAITING_NEXT 2 //definição de estado: esperandoAproxima
#define WAITING_S2B 3 //definição de estado: esperandoSensor2B
#define CALC_AND_SHOW 4 //definição de estado: calculandoEmostrando

#define HUD_D 5 //definição de estado do display: display 1 - Hud
#define MENU_D 6 //definição de estado do display: display 2 - Menu
#define SET_D 7 //definição de estado do display: display 3 - Settings
#define STATS_D 8 //definição de estado do display: display 4 - Stats

#define MENU_BTN 1 //definição de botão: Menu
#define ESC_BTN 3 //definição de botão: Cancelar
#define UP_BTN 4 //definição de botão: Up
#define SLC_BTN 5 //definição de botão: Select
#define DOWN_BTN 8 //definição de botão: Down
#define LIGHT_BTN 9 //definição de botão: Light

#define maxUlong 7468 //tempo de espera para medir 1 Km/h. Velocidade menor do que essa não é exibida
#define defWhellSize 2.074 //comprimento do pneu em metros; 26"x0.0254

#define rotationAddr 0 //endereço da EEPROM que guarda o número de rotações
#define wheelAddr 4 //endereço da EEPROM que guarda o tamanho da roda
#define mDstAddr 8 //endereço da EEPROM que guarda distância máxima percorrida
#define mSpdAddr 12 //endereço da EEPROM que guarda velocidada máxima alcançada
#define limitTimeAddr 16 //endereço da EEPROM que guarda tempo de espera para medir 1 Km/h

#define LIGHT_PIN 7 //Definição do pino de iluminação (LED)
#define SCL_PIN 8 //Definição do pino de clock do teclado.
#define SDO_PIN 9 //Definição do pino de dados do teclado.

// Grandezas Físicas
double wheelLength = defWhellSize; //comprimento da circuferência em metros.
unsigned int wlInMilis = 0; //comprimento da circuferência em milímetros.
unsigned long int wlSize = 0; //comprimento da circuferência em milímetros.
long unsigned int rotation = 0; //número de voltas.
long unsigned int rotationAr = 0; //número de voltas armazenado.
long unsigned int stepTimeCounter = 0; //contador de tempo em milissegundos.
long unsigned int limitTime = maxUlong; //tempo de espera para medir 1 Km/h
double dist = 0; //distância atual percorrida.
double distAr = 0; //distância armazenada.
double totalDist = 0; // = distância atual + distância armazenada.
double mDst = 0; //máxima distância armazenada.
unsigned int vi = 0; //velocidade instantânea em km/h.
unsigned int mSpd = 0; //máxima velocidade armazenada.
unsigned int tac = 0; //quantidade de rotações por minuto.

//Marcadores de Eventos
uint8_t hadPortDropped0 = 0; //sinaliza a interrupção no pino 2.
uint8_t hadPortDropped1 = 0; //sinaliza a interrupção no pino 3.
uint8_t button = 0; //indica o botão que foi pressionado.

//Variáveis de estado
uint8_t state = STOPPED; //estado do sistema de medição
uint8_t stateDsp = HUD_D; //estado de exibição do display
uint8_t dispUpdate = LOW; //indica a presença de atualizações no display - evita chamar rotina de visualização sem necessidade
char lightState = 'x'; //indica estado da sinalização ON/OFF

//Variáveis auxiliares
uint8_t wlDig[4] = {0,0,0,0}; //vetor de armazenamento temporário dos dígitos do tamanho da roda.
uint8_t unitDig = 0; //indica qual dígito do tamanho da roda está sendo editado
uint8_t UpDownOK = 0; //libera botões Up e Down
uint8_t menuOp = 0; //opção selecionada do menu

//Objetos de bibliotecas
Adafruit_SSD1306 display(OLED_RESET); //objeto do display
TTP229 ttp229(SCL_PIN, SDO_PIN); //objeto do teclado

//funções da EEPROM
void EEPROMWritelong(int address, long value){
    //Decomposition from a long to 4 bytes by using bitshift.
    //One = Most significant -> Four = Least significant byte
    byte four = (value & 0xFF);
    byte three = ((value >> 8) & 0xFF);
    byte two = ((value >> 16) & 0xFF);
    byte one = ((value >> 24) & 0xFF);

    //Write the 4 bytes into the eeprom memory.
    EEPROM.write(address, four);
    EEPROM.write(address + 1, three);
    EEPROM.write(address + 2, two);
    EEPROM.write(address + 3, one);
}

long EEPROMReadlong(long address){
    //Read the 4 bytes from the eeprom memory.
    long four = EEPROM.read(address);
    long three = EEPROM.read(address + 1);
    long two = EEPROM.read(address + 2);
    long one = EEPROM.read(address + 3);

    //Return the recomposed long by using bitshift.
    return ((four << 0) & 0xFF) + ((three << 8) & 0xFFFF) + ((two << 16) & 0xFFFFFF) + ((one << 24) & 0xFFFFFFFF);
}

//interrupção de tempo
void timeHandler(){
    stepTimeCounter++;
}

//primeira interrupção - sensor 1
void portHandler0(){
    if(state == STOPPED || state == WAITING_NEXT){
        hadPortDropped0 = HIGH;
    }
}

//segunda interrupção - sensor 2
void portHandler1(){
    if(state == WAITING_S2A || state == WAITING_S2B){
        hadPortDropped1 = HIGH;
    }
}

//determina tempo de espera para medir 1 Km/h - retorna tempo em ms
long unsigned int waitingTime(double wl){
return (wl/(1/3.6))*1000.0; // 1/3.6 >> para v = 1km/h
}

//funções velocidade
unsigned int getSpeed(unsigned long int time, double wl){
    return (wl/(time/1000.0))*3.6;
}

//função distância
double getDistance(unsigned int rot, double wl){
    return (wl*rot);
}

//função tacômetro
unsigned int getRPM(unsigned long int time){
	return 60000/time;
}

//preenche vetor que armazena tamanho da roda
void fillVector(uint8_t vec[4], unsigned int wlSizeMM){
    uint8_t wlSlc = 0;
    for(wlSlc = 4 ; wlSlc > 0 ; wlSlc--){
        vec[wlSlc-1] = wlSizeMM%10;
        wlSizeMM /= 10;
    }	
}

//função auxiliar do display - pula linha
void dispThinLine(uint8_t fontSize){
	display.setTextSize(1);
	display.println("");
    display.setTextSize(fontSize);
}

//indica o digíto do tamanho da roda a ser alterado
void slcDig(int unit){
    if(unit == 1){
        display.setTextColor(BLACK, WHITE);
    }    
    display.print(wlDig[0]);
    display.setTextColor(WHITE);
    if(unit == 2){
        display.setTextColor(BLACK, WHITE);
    }            
    display.print(wlDig[1]);
    display.setTextColor(WHITE);
    if(unit == 3){
        display.setTextColor(BLACK, WHITE);
    }
    display.print(wlDig[2]);
    display.setTextColor(WHITE);
    if(unit == 4){
        display.setTextColor(BLACK, WHITE);
    }
    display.print(wlDig[3]);
    display.setTextColor(WHITE);
}

//Mostra display 1 - Hud
void hudDisplay (){
	display.clearDisplay();
    display.setTextColor(WHITE);
    display.setCursor(0,0);
	display.setTextSize(1);
	display.print("Signaling: ");
	display.println(lightState);
    dispThinLine(2);
	display.print(vi);
	display.println("km/h");
	display.print(dist);
	display.println("m");
    dispThinLine(1);
	display.print(tac);
	display.println(" RPM"); 
	display.display(); 
}

//Mostra display 2 - Menu
void menuDisplay(){
	display.clearDisplay();
    display.setCursor(0,0);
	display.setTextSize(2);
	display.print("~~~MENU~~~");
    dispThinLine(2);
    if(menuOp == 0){
        display.setTextColor(BLACK, WHITE);
        display.println(">Settings");
        dispThinLine(2);
        display.setTextColor(WHITE);
        display.println(">Stats");
    }else if(menuOp == 1){
        display.println(">Settings");
        dispThinLine(2);
        display.setTextColor(BLACK, WHITE);
        display.println(">Stats");
        display.setTextColor(WHITE);
    }
    display.display();
}

//Mostra display 3 - Settings
void setDisplay(){
	display.clearDisplay();
    display.setTextColor(WHITE);
    display.setCursor(0,0);
	display.setTextSize(2);
	display.print("~Settings~");
    dispThinLine(2);
	display.print("Wheel Size");
    dispThinLine(2);
	slcDig(unitDig);
	display.println("mm");
	display.display(); 	
}	

//Mostra display 4 - Stats
void statsDisplay(){
	display.clearDisplay();
    display.setTextColor(WHITE);
    display.setCursor(0,0);
	display.setTextSize(2);
	display.print(" ~~Stats~~");
    dispThinLine(1);
	display.print("TotalDist:");
	display.print(totalDist);
	display.println("m");
    dispThinLine(1);
	display.print("MaxDist:");
	display.print(mDst);	
	display.println("m");
    dispThinLine(1);
	display.print("MaxSpeed:");
	display.print(mSpd);
	display.println("km/h");
	display.display();
}
	
void setup() {
 
    Serial.begin(115200); //baud rate
    Timer1.initialize(1000); //a cada 1000us chama a interrupção de tempo (1ms)
    Timer1.attachInterrupt(timeHandler); //timeHandler é executado quando ocorre interrupção de tempo
    attachInterrupt(0, portHandler0, FALLING); //pino 2: portHandler0 é executado quando ocorre interrupção de descida
    attachInterrupt(1, portHandler1, FALLING); //pino 3: portHandler1 é executado quando ocorre interrupção de descida
    
	limitTime = EEPROMReadlong(limitTimeAddr); //faz a leitura do tempo de espera
    wlSize = EEPROMReadlong(wheelAddr); //faz a leitura do tamanho da roda armazenado na EEPROM (mm)
    fillVector(wlDig, wlSize); //preenche vetor do tamanho da roda com o valor armazenado na EEPROM (mm)  
    Serial.println(wlSize);
    Serial.println(wheelLength);
    wheelLength = wlSize/1000.0; //tamanho da roda em metros	    
    Serial.println(wheelLength);
	Serial.println(limitTime);

    rotationAr = EEPROMReadlong(rotationAddr); //faz a leitura do número de rotações armazenado na EEPROM
    distAr = wheelLength*rotationAr; //calcula a distância armazenada
    totalDist = distAr; //atualiza a distância total com o valor do que já foi percorrido até o momento
	mDst = EEPROMReadlong(mDstAddr);
	mSpd = EEPROMReadlong(mSpdAddr);
	
    Serial.print("Rotations: ");
    Serial.print(rotationAr);
    Serial.print("Distance: ");
    Serial.print(distAr);
    Serial.println("m;");
	
    pinMode(LIGHT_PIN, OUTPUT);
    digitalWrite(LIGHT_PIN, LOW);
    
    // by default, we'll generate the high voltage from the 3.3v line internally! (neat!)
    display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x64)
    // init done
	
	hudDisplay();
}

void loop() {
    
    button = ttp229.GetKey16();
	if(button != 0){
        Serial.println(button);
        dispUpdate = HIGH;
        while(ttp229.GetKey16()){}
    }

	//transição de estados
    switch(state){
        case STOPPED:
            if(hadPortDropped0){ // transition stopped => waitingS2A
                hadPortDropped0 = LOW;
                state = WAITING_S2A;
                stepTimeCounter = 0;
            }
            break;
        case WAITING_S2A:
            if(hadPortDropped1){ // transition waitingS2A => waitingNext
                hadPortDropped1 = LOW;
                state = WAITING_NEXT;
                stepTimeCounter = 0;
            }
            else{
                if(stepTimeCounter > limitTime){// transition waitingS2A => stopped
                    state = STOPPED;
                    vi = 0;
                    tac = 0;
                    EEPROMWritelong(rotationAddr, rotation + rotationAr);
                    Serial.println("Guardado!");
                    dispUpdate = HIGH;
                }
            }
            break;
        case WAITING_NEXT:
            if(hadPortDropped0){// transition waitingNext => waitingS2B
                hadPortDropped0 = LOW;
                state = WAITING_S2B;
            }
            else{
                if(stepTimeCounter > limitTime){// transition waitingNext => stopped
                    state = STOPPED;
                    vi = 0;
                    tac = 0;
                    EEPROMWritelong(rotationAddr, rotation + rotationAr);
                    Serial.println("Guardado!");
                    dispUpdate = HIGH;
                }
            }
            break;
        case WAITING_S2B:
            if(hadPortDropped1){// transition waitingS2B => calcAndShow
                hadPortDropped1 = LOW;
                state = CALC_AND_SHOW;
            }
            else{
                if(stepTimeCounter > limitTime){// transition waitingS2B => stopped
                    state = STOPPED;
                    vi = 0;
                    tac = 0;
                    EEPROMWritelong(rotationAddr, rotation + rotationAr);
                    Serial.println("Guardado!");
                    dispUpdate = HIGH;
                }
            }
            break;
        case CALC_AND_SHOW:
            state = WAITING_NEXT;
            dispUpdate = HIGH;
            rotation++;
            vi = getSpeed(stepTimeCounter, wheelLength);
			if(vi >= mSpd){
				mSpd = vi;
				EEPROMWritelong(mSpdAddr, mSpd);
			}
            dist = getDistance(rotation, wheelLength);
			if(dist >= mDst){
				mDst = dist;
				EEPROMWritelong(mDstAddr, mDst);
			}
            totalDist = dist + distAr;
			tac = getRPM(stepTimeCounter);
            stepTimeCounter = 0;
            break;
        default:
            Serial.println("Estado indefinido;");
            break;
    }

	//transição de estados do display
    if(dispUpdate){
        if(button == LIGHT_BTN){
			Serial.println("lght");
            if(lightState == 'x'){
                lightState = 'o';
                digitalWrite(LIGHT_PIN, HIGH);
            }else{
                lightState = 'x';
                digitalWrite(LIGHT_PIN, LOW);
            }
        }
        switch(stateDsp){
            case HUD_D:	
                if(button == MENU_BTN){
                    Serial.println("bMn");
                    stateDsp = MENU_D;
                }
                button = 0;
                if(stateDsp == HUD_D){
                    hudDisplay();
                    dispUpdate = LOW;
                }
                break;
            case MENU_D:
                switch(button){
                    case ESC_BTN:
                        Serial.println("esc");
                        stateDsp = HUD_D;
                        break;
                    case SLC_BTN:
                        Serial.println("Slc");
                        if(menuOp == 0){
                            stateDsp = SET_D;
                        }
                        if(menuOp == 1){
                            stateDsp = STATS_D;
                        }
                        break;
                    case DOWN_BTN:
                        Serial.println("down");
                        menuOp = 1;
                        break;
                    case UP_BTN:
                        Serial.println("up");
                        menuOp = 0;
                        break;
                    default:
                        break;
                }
                button = 0;
                if(stateDsp == MENU_D){
                    menuDisplay();
                    dispUpdate = LOW;
                }
                break;
            
            case SET_D:
                switch(button){
                    case ESC_BTN:
                        Serial.println("esc");
						unitDig = 0;                        
						if(wlSize == 0){
							fillVector(wlDig, wlInMilis);	
						}else if(wlSize != 0){
							fillVector(wlDig, wlSize);
						}
                        stateDsp = HUD_D;
                        break;
                    case MENU_BTN:
                        Serial.println("bMn");
						unitDig = 0;
						if(wlSize == 0){
							fillVector(wlDig, wlInMilis);	
						}else if(wlSize != 0){
							fillVector(wlDig, wlSize);
						}
						stateDsp = MENU_D;
                        break;    
                    case SLC_BTN:
                        Serial.println("Slc");
						if(unitDig == 0){
							UpDownOK = 1;
							unitDig = 1;
						}else if(unitDig == 1){
							unitDig = 2;
						}else if(unitDig == 2){
							unitDig = 3;
						}else if(unitDig == 3){
							unitDig = 4;
						}else if(unitDig == 4){
							wlSize = (wlDig[0]*1000) + (wlDig[1]*100) + (wlDig[2]*10) + wlDig[3];
                            EEPROMWritelong(wheelAddr, wlSize);                            
                            wheelLength = wlSize/1000.0;
							limitTime = waitingTime(wheelLength);
							EEPROMWritelong(limitTimeAddr, limitTime);
							unitDig = 0;
							totalDist = 0;
							distAr = 0;
							mDst = 0;
							mSpd= 0;
							Serial.println(wlSize);
							Serial.println(wheelLength);
							Serial.println(limitTime);							
						}
						break;
					case DOWN_BTN:
						Serial.println("down");
						if(UpDownOK == 1){
							wlDig[unitDig-1]--;
							if(wlDig[unitDig-1] == 255){
								wlDig[unitDig-1] = 9;
							}									
						}
                        break;
					case UP_BTN:
						Serial.println("up");
						if(UpDownOK == 1){
							wlDig[unitDig-1]++;
							if(wlDig[unitDig-1] == 10){
								wlDig[unitDig-1] = 0;
							}									
						}						
                        break;                   
					default:
                        break;
                }
                button = 0;
                if(stateDsp == SET_D){
                    setDisplay();
                    dispUpdate = LOW;
                }
                break;
            
            case STATS_D:
                switch(button){
                    case ESC_BTN:
                        Serial.println("esc");
                        stateDsp = HUD_D;
                        break;
                    case MENU_BTN:
                        Serial.println("bMn");
                        stateDsp = MENU_D;
                        break;
                    default:
                        break;
                }
                button = 0;
                if(stateDsp == STATS_D){
                    statsDisplay();
                    dispUpdate = LOW;
                }
                break;
            
            default:
                Serial.println("Estado indefinido");
                button = 0;
                break;
        }
    }
}

